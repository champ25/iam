import boto3, sys, datetime, time
def handler(event, context):
    iam_client = boto3.client('iam')
    user_list=[]
    group_list=[]
    whitelist_group_name="automation-users"
    response = iam_client.list_users()
    for item in response['Users']:
        user = item['UserName']
        flag=False
        user_list.append(user)
        response = iam_client.list_groups_for_user(UserName=user)
        if flag==True:
            flag=True
            continue                    
        else:
            cleanup(user,iam_client)
def cleanup(user,iam_client):
    response = iam_client.list_access_keys(UserName=user)
    for key in response['AccessKeyMetadata']:
        create_date = time.mktime(key['CreateDate'].timetuple())
        now = time.time()
        age = (now - create_date) // 86400
        if age > 90:
            response = iam_client.delete_access_key(
                UserName=user,
                AccessKeyId=key['AccessKeyId']
            )